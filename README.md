Flip-Clock
==========

An Add-to-Homescreen App for IOS. Dock a useless first-gen iPod Touch & use it as a bedside clock.

Attribution
===========

I cobbled this together from found flip-clock code which I can't seem to refind. Not-my-Javascript.

My contribution was only to stack the hours and minutes, hide the seconds, and create a silly iOS app icon for it.

I use this during the day when my real phone isn't docked next to my bed to have a bedroom flip-clock.

Installation
============

1. Host it somewhere, or use mine: http://flipclock.grantstavely.com/clock.html
2. Add it to your home screen thusly: https://www.apple.com/ios/add-to-home-screen/
3. You're done.

